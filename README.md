g131210055 -- Ali Fırat KUAS
g131210091 -- Doğuhan Aybars AY

Projemiz ikinci el kitap satışının sağlandığı bir web sayfası olacak. Admin ve kullanıcı girişleri farklı olup admin girişinde site üzerinde yetkilendirmesi farklı olan sayfa tasarımı getirilecek. Kullanıcı girişinde ise kitap alıp satmak için farklı bir giriş olup mesaj panosu eklenecek. Site içerisinden herhangi bir şekilde para alışverişi olmayacak. Alıcı ve satıcı mesaj panosundan birbirleriyle anlaşıp yüzyüze alışverişi gerçekleştirecek. 


Satılacak olan kitabın ne kadar hasar aldığı ya da kaç senesinde alındığı gibi bilgiler satılacak ürünün açıklama kısmına eklenmesini sağlayan bir yapı oluşturulacak. Satın almak için siteye giren kişiler için ise alacakları kitapları yeniden eskiye doğru sıralayan bir sistem koyulacak. Hakeza bu sistem opsiyonel olup fiyata göre de değiştirilebilir. Aynı e-maille birden fazla kullanıcı oluşturulamayacak. İlerisi için ise haritalarda aynı şehirde olan insanlar birbirleriyle eşleşecek.


 Eğer isterlerse başka şehirden arama yapabilirler. Satıcı için oylama sistemi getirilecek ve buna sadece bir ürün satın alan insanlar kullanabilecek. Son olarak da her profil için profil düzenleme seçeneği eklenecek.

